#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <vector>

// Select the platform
#include <FCam/N950.h>
#include <FCam/AutoWhiteBalance.h>
namespace Plat = FCam::N950;

/** \file */

/***********************************************************/
/* A program that takes a single shot                      */
/*                                                         */
/* This example is a simple demonstration of the usage of  */
/* the FCam API.                                           */
/***********************************************************/

void errorCheck();

int main(int argc, char **argv) {

    unsigned long exposure;

    if (argc < 2) {
        printf("Wrong number of args\n");
        return 1;
    }

    exposure = strtoul(argv[1], NULL, 0);
    if (exposure == 0) {
        return 1;
    }

    // Make the image sensor
    Plat::Sensor sensor;
    printf("Sensor created\n");

    // Make a new shot
    std::vector<FCam::Shot> shot(3);
    for (size_t i = 0; i < shot.size(); i++) {
        shot[i].exposure = (int)exposure; // 50 ms exposure
        shot[i].gain = 1.0f;      // minimum ISO
    }
    printf("Shots setup\n");

    // Specify the output resolution and format, and allocate storage for the resulting image
    shot[0].image = FCam::Image(4016, 2242, FCam::RAW);
    shot[1].image = FCam::Image(4016, 2242, FCam::RAW);
    shot[2].image = FCam::Image(4016, 2242, FCam::RAW);
    shot[0].histogram.enabled = true;
    shot[0].histogram.region = FCam::Rect(0, 0, 4016, 2242);
    shot[1].histogram.enabled = true;
    shot[1].histogram.region = FCam::Rect(0, 0, 4016, 2242);
    shot[2].histogram.enabled = true;
    shot[2].histogram.region = FCam::Rect(0, 0, 4016, 2242);
    shot[0].sharpness.enabled = true;
    shot[1].sharpness.enabled = true;
    shot[2].sharpness.enabled = true;
    printf("Shot size set\n");

    printf("Starting capture\n");
    // Order the sensor to capture the shots
    sensor.capture(shot);

    printf("Checking for errors\n");
    // Check for errors
    errorCheck();

    printf("Shots pending ?\n");
    assert(sensor.shotsPending() == shot.size()); // There should be exactly this many shots

    printf("Getting the frames\n");
    // Retrieve the frame from the sensor
    std::vector<FCam::Frame> frame(shot.size());
    for (size_t i = 0; i < shot.size(); i++) {
        frame[i] = sensor.getFrame();
    }

    printf("Stopping sensor\n");
    sensor.stop();

    printf("Checking for errors\n");
    errorCheck();

    printf("Checking frames\n");
    // Each frame should be the result of the shot we made
    for (size_t i = 0; i < shot.size(); i++) {
        printf("Checking frame %d\n", i);
        assert(frame[i].shot().id == shot[i].id);

        // This frame should be valid too
        assert(frame[i].valid());
        assert(frame[i].image().valid());
        printf("Frame %d OK!\n", i);

        printf("AutoWhiteBalancing frame %d\n", i);
        autoWhiteBalance(&shot[i], frame[i]);
    }

    printf("Saving frames\n");
    // Save the frames
    FCam::saveDNG(frame[0], "/home/nemo/frame0.dng");
    FCam::saveDNG(frame[1], "/home/nemo/frame1.dng");
    FCam::saveJPEG(frame[2], "/home/nemo/frame2.jpg");

    // Check that the pipeline is empty
    assert(sensor.framesPending() == 0);
    assert(sensor.shotsPending() == 0);

    return 0;
}




void errorCheck() {
    // Make sure FCam is running properly by looking for DriverError
    FCam::Event e;
    while (FCam::getNextEvent(&e, FCam::Event::Error)) {
        printf("Error: %s\n", e.description.c_str());
        if (e.data == FCam::Event::DriverMissingError) {
            printf("example1: FCam can't find its driver. Did you install "
                   "fcam-drivers on your platform, and reboot the device "
                   "after installation?\n");
            exit(1);
        }
        if (e.data == FCam::Event::DriverLockedError) {
            printf("example1: Another FCam program appears to be running "
                   "already. Only one can run at a time.\n");
            exit(1);
        }
    }
}
