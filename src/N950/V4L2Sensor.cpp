#include <linux/videodev2.h>
#include <linux/v4l2-subdev.h>
#include <linux/omap3isp.h>
#include <asm/types.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <sys/prctl.h>
#include <linux/capability.h>

#include <pthread.h>
#include <poll.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

#include <sys/fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <time.h>

#include <errno.h>
#include <malloc.h>

#include "../Debug.h"
#include "V4L2Sensor.h"
#include "linux/isp_user.h"
#include "linux/omap34xxcam-fcam.h"
#include "FCam/Event.h"


namespace FCam {
namespace N950 {

V4L2Sensor *V4L2Sensor::instance(std::string fname) {
    std::map<std::string, V4L2Sensor *>::iterator i;
    i = instances_.find(fname);
    if (i == instances_.end()) {
        instances_[fname] = new V4L2Sensor(fname);
    }

    return instances_[fname];
};

V4L2Sensor::V4L2Sensor(std::string fname) : state(CLOSED), filename(fname) {
    fd = ::open(filename.c_str(), O_RDWR | O_NONBLOCK, 0);

    if (fd < 0) {
        error(Event::DriverError, "V4L2Sensor: Error opening %s: %s", filename.c_str(), strerror(errno));
        return;
    }
    enumerateEntities();
}

std::map<std::string, V4L2Sensor *> V4L2Sensor::instances_;

void V4L2Sensor::open() {
    if (state != CLOSED) {
        return;
    }


    sensorfd = ::open(sensorPath.c_str(), O_RDWR | O_NONBLOCK, 0);

    if (sensorfd < 0) {
        error(Event::DriverError, "V4L2Sensor: Error opening %s: %s", sensorPath.c_str(), strerror(errno));
        return;
    }

    lensfd = ::open(lensPath.c_str(), O_RDWR | O_NONBLOCK, 0);

    if (lensfd < 0) {
        error(Event::DriverError, "V4L2Sensor: Error opening %s: %s", lensPath.c_str(), strerror(errno));
        return;
    }

    flashfd = ::open(flashPath.c_str(), O_RDWR | O_NONBLOCK, 0);

    if (flashfd < 0) {
        error(Event::DriverError, "V4L2Sensor: Error opening %s: %s", flashPath.c_str(), strerror(errno));
        return;
    }

    outputfd = ::open("/dev/video2", O_RDWR | O_NONBLOCK, 0);

    if (outputfd < 0) {
        error(Event::DriverError, "V4L2Sensor: Error opening /dev/video2: %s", strerror(errno));
        return;
    }

    ccdcfd = ::open("/dev/v4l-subdev2", O_RDWR | O_NONBLOCK, 0);

    if (ccdcfd < 0) {
        error(Event::DriverError, "V4L2Sensor: Error opening /dev/v4l-subdev2: %s", strerror(errno));
        return;
    }

    histfd = ::open("/dev/v4l-subdev7", O_RDWR | O_NONBLOCK, 0);

    if (ccdcfd < 0) {
        error(Event::DriverError, "V4L2Sensor: Error opening /dev/v4l-subdev7: %s", strerror(errno));
        return;
    }

    affd = ::open("/dev/v4l-subdev6", O_RDWR | O_NONBLOCK, 0);

    if (affd < 0) {
        error(Event::DriverError, "V4L2Sensor: Error opening /dev/v4l-subdev6: %s", strerror(errno));
        return;
    }
    state = IDLE;
}

void V4L2Sensor::close() {
    if (state != CLOSED) {
        ::close(fd);
        ::close(sensorfd);
        ::close(lensfd);
        ::close(flashfd);
        ::close(outputfd);
        ::close(ccdcfd);
        ::close(histfd);
        ::close(affd);
    }
    state = CLOSED;
}

int V4L2Sensor::getFD() {
    if (state == CLOSED) {
        return -1;
    }
    return fd;
}

int V4L2Sensor::getCCDCFd() {
    if (state == CLOSED) {
        return -1;
    }
    return ccdcfd;
}

std::string V4L2Sensor::pathFromEntity(struct Entity e) {
    std::string sysPath;
    char sysbuf[32];
    char buf[1024];
    int ret;

    sprintf(sysbuf, "/sys/dev/char/%d:%d", e.info.v4l.major, e.info.v4l.minor);
    ret = ::readlink(sysbuf, buf, sizeof(buf)-1);
    if (ret < 0)
        return std::string();

    buf[ret] = '\0';
    sysPath = std::string(buf);
    return std::string("/dev")+sysPath.substr(sysPath.find_last_of("/"));

}

void V4L2Sensor::enumerateEntities() {
    int id=0;
    int i;
    dprintf(3, "Enumerating entities\n");

    while(true) {
        struct Entity e;
        memset (&e.info, 0, sizeof(media_entity_desc));
        e.info.id = id | MEDIA_ENT_ID_FLAG_NEXT;
        if (ioctl(fd, MEDIA_IOC_ENUM_ENTITIES, &e.info) < 0)
            break;

        id = e.info.id;

        if (e.info.type != MEDIA_ENT_T_DEVNODE_V4L &&
               e.info.type != MEDIA_ENT_T_V4L2_SUBDEV) 
            continue;

        if (std::string(e.info.name).find("jt8ev1") == 0) {
            sensorPath = pathFromEntity(e);
            dprintf(3, "Found sensor %s, dev path %s\n", e.info.name, sensorPath.c_str());
        }

        if (std::string(e.info.name).find("ad5836") == 0) {
            flashPath = pathFromEntity(e);
            dprintf(3, "Found flash %s, dev path %s\n", e.info.name, flashPath.c_str());
        }

        if (std::string(e.info.name).find("as3645a") == 0) {
            lensPath = pathFromEntity(e);
            dprintf(3, "Found lens %s, dev path %s\n", e.info.name, lensPath.c_str());
        }

        dprintf(3, "Adding new entity %s\n", e.info.name);

        struct media_pad_desc pads[e.info.pads];
        struct media_link_desc links[e.info.links];
        struct media_links_enum linksenum;
        linksenum.entity = e.info.id;
        linksenum.pads = pads;
        linksenum.links = links;

        if (ioctl(fd, MEDIA_IOC_ENUM_LINKS, &linksenum) < 0) {
            dprintf(3, "The requested entity %d doesn't exist\n", e.info.id);
        }

        for (i = 0; i < e.info.pads; i++) {
            struct media_pad_desc pad = linksenum.pads[i];
            e.pads[pad.index] = pad;
        }

        for (i = 0; i < e.info.links; i++) 
            e.links[i] = linksenum.links[i];

        entities[e.info.id] = e;
    }
}

bool V4L2Sensor::setupPipeline(std::string pipeline) {
    return true;
}

void V4L2Sensor::enumerateSensorFormats() {
    struct v4l2_subdev_mbus_code_enum format;
    struct v4l2_subdev_frame_size_enum size;
    int n;

    dprintf(3, "Probing sensor formats\n");

    /* format enumeration */
    for (n = 0;; n++) {

        /* It has only one pad */
        format.pad = 0;
        format.index = n;

        if (ioctl (sensorfd, VIDIOC_SUBDEV_ENUM_MBUS_CODE, &format) < 0) {
            if (errno == EINVAL) {
                break;                  /* end of enumeration */
            } else {
                return;
            }
        }

        dprintf(3,"got code: %d\n", format.code);

        size.index = 0;
        size.pad = 0;               /* Sensor has always only one pad */
        size.code = format.code;

        
        dprintf(3,"querying frame size, index = %d, pad = %d, code = %d\n",
                size.index, size.pad, size.code);

        if (ioctl (sensorfd, VIDIOC_SUBDEV_ENUM_FRAME_SIZE, &size) < 0) {
            dprintf(3,"Couldn't enum frame sizes\n");
            return;
        }

        do {
            dprintf(3, "Got resolution: %ux%u - %ux%u\n",
                    size.min_width, size.min_height, size.max_width, size.max_height);
            size.index++;
        } while (ioctl (sensorfd, VIDIOC_SUBDEV_ENUM_FRAME_SIZE, &size) >= 0);
    }

}

void V4L2Sensor::startStreaming(Mode m,
                                const HistogramConfig &histogram,
                                const SharpnessMapConfig &sharpness) {

    if (state != IDLE) {
        error(Event::InternalError, "V4L2Sensor: Can only initiate streaming if sensor is idle");
        return;
    }

    struct v4l2_format fmt;
    

    enumerateSensorFormats();
    memset(&fmt, 0, sizeof(struct v4l2_format));

    fmt.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    fmt.fmt.pix.width       = m.width;
    fmt.fmt.pix.height      = m.height;
    if (m.type == UYVY) {
        fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_UYVY;
    } else if (m.type == RAW) {
        fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_SGRBG10;
    } else {
        error(Event::InternalError, "V4L2Sensor: Unknown image format requested");
        return;
    }
    fmt.fmt.pix.field       = V4L2_FIELD_NONE;

    // Request format
    if (ioctl(outputfd, VIDIOC_S_FMT, &fmt) < 0) {
        error(Event::DriverError, "VIDIOC_S_FMT");
        return;
    }

    // Try for the right frame rate, to be robust to other programs messing with the modes
    struct v4l2_streamparm parm;
    memset(&parm, 0, sizeof(struct v4l2_streamparm));
    parm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    parm.parm.capture.timeperframe.numerator = 0;
    parm.parm.capture.timeperframe.denominator = 1000000;
    /*
    if (m.height <= 960) {
        parm.parm.capture.timeperframe.numerator = 33000;
        parm.parm.capture.timeperframe.denominator = 1000000;
    } else {
        parm.parm.capture.timeperframe.numerator = 77000;
        parm.parm.capture.timeperframe.denominator = 1000000;
    }
    */
    /*if (ioctl(sensorfd, VIDIOC_S_PARM, &parm) < 0) {
        error(Event::DriverError, "VIDIOC_S_PARM");
        return;
    }*/

    currentMode.width = fmt.fmt.pix.width;
    currentMode.height = fmt.fmt.pix.height;
    currentMode.type = (fmt.fmt.pix.pixelformat == V4L2_PIX_FMT_UYVY) ? UYVY : RAW;

    struct v4l2_requestbuffers req;
    memset(&req, 0, sizeof(req));
    req.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_MMAP;
    req.count  = 8;

    if (ioctl(outputfd, VIDIOC_REQBUFS, &req) < 0) {
        error(Event::DriverError, "VIDIOC_REQBUFS: %s", strerror(errno));
        return;
    }

    buffers.resize(req.count);

    for (size_t i = 0; i < buffers.size(); i++) {
        v4l2_buffer buf;
        memset(&buf, 0, sizeof(v4l2_buffer));
        buf.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;
        buf.index  = i;

        if (ioctl(outputfd, VIDIOC_QUERYBUF, &buf) < 0) {
            error(Event::DriverError, "VIDIOC_QUERYBUF: %s", strerror(errno));
            return;
        }

        buffers[i].index = i;
        buffers[i].length = buf.length;
        buffers[i].data =
            (unsigned char *)mmap(NULL, buffers[i].length, PROT_READ | PROT_WRITE,
                                  MAP_SHARED, outputfd, buf.m.offset);

        if (buffers[i].data == MAP_FAILED) {
            error(Event::InternalError, "V4L2Sensor: mmap failed: %s", strerror(errno));
            return;
        }
    }

    for (size_t i = 0; i < buffers.size(); i++) {
        releaseFrame(&buffers[i]);
    }

    // set the starting parameters
    setHistogramConfig(histogram);
    setSharpnessMapConfig(sharpness);

    enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (ioctl(outputfd, VIDIOC_STREAMON, &type) < 0) {
        error(Event::DriverError, "VIDIOC_STREAMON: %s", strerror(errno));
        return;
    }

    dprintf(2, "Sensor now streaming\n");
    state = STREAMING;
}


void V4L2Sensor::stopStreaming() {
    if (state != STREAMING) {
        return;
    }

    enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (ioctl(outputfd, VIDIOC_STREAMOFF, &type) < 0) {
        error(Event::DriverError, "VIDIOC_STREAMOFF: %s", strerror(errno));
        return;
    }

    for (size_t i = 0; i < buffers.size(); i++) {
        if (munmap(buffers[i].data, buffers[i].length)) {
            error(Event::InternalError, "munmap failed: %s", strerror(errno));
        }
    }

    state = IDLE;
}



V4L2Sensor::V4L2Frame *V4L2Sensor::acquireFrame(bool blocking) {
    if (state != STREAMING) {
        error(Event::InternalError, "V4L2Sensor: Can't acquire a frame when not streaming");
        return NULL;
    }

    v4l2_buffer buf;
    memset(&buf, 0, sizeof(v4l2_buffer));
    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;

    if (blocking) {
        struct pollfd p = {outputfd, POLLIN, 0};
        poll(&p, 1, -1);
        if (!(p.revents & POLLIN)) {
            error(Event::DriverError, "Poll returned without data being available: %s", strerror(errno));
            return NULL;
        }
    }

    if (ioctl(outputfd, VIDIOC_DQBUF, &buf) < 0) {
        if (errno == EAGAIN && !blocking) {
            return NULL;
        }

        error(Event::DriverError, "VIDIOC_DQBUF: %s", strerror(errno));
        return NULL;
    }

    buffers[buf.index].processingDoneTime = Time(buf.timestamp);
    return &(buffers[buf.index]);
}



Histogram V4L2Sensor::getHistogram(Time t, const HistogramConfig &conf) {

    if (!conf.enabled) {
        return Histogram();
    }

    // grab the histogram data for this frame

    //struct isp_hist_data hist_data;
    struct omap3isp_stat_data hist_data;
    unsigned buf[hist.buf_size];
    hist_data.buf = buf;
    hist_data.buf_size = hist.buf_size;
    //hist_data.update = REQUEST_STATISTICS;

    // For now we assume acquire_frame is being called quickly
    // enough that only the newest frame is relevant
    hist_data.frame_number = NEWEST_FRAME;
    hist_data.cur_frame = 0;
    hist_data.config_counter = 0;
    hist_data.ts.tv_sec = 0;
    hist_data.ts.tv_usec = 0;

    if (ioctl(histfd, VIDIOC_OMAP3ISP_STAT_REQ, &hist_data)) {
        if (errno != EBUSY) {
            error(Event::DriverError, "VIDIOC_OMAP3ISP_STAT_REQ: %s", strerror(errno));
        }
        return Histogram();
    }

    if (hist_data.config_counter != hist.config_counter) {
        dprintf(3, "Wrong histogram config\n");
        return Histogram();
    }

    dprintf(3, "Got histogram data : frame number : %d\n", hist_data.frame_number);


    // TODO: Deal with timestamps that are too early or too late

    Time h(hist_data.ts);
    h += 80000;

    dprintf(3, "Got histogram with timestamp %s, comparing to %s\n", h.toString().c_str(), t.toString().c_str()); 

    if ((t - h) > 4000) {
        warning(Event::DriverError, "Missing histogram (%d)\n", t-h);
        return Histogram();
    }

    while ((t-h) < -4000) {
        // we got the wrong histogram!
        if (hist_data.frame_number == 0) { hist_data.frame_number = 4095; }
        else { hist_data.frame_number--; }

        if (ioctl(histfd, VIDIOC_OMAP3ISP_STAT_REQ, &hist_data)) {
            if (errno != EBUSY)
                error(Event::DriverError, "VIDIOC_OMAP3ISP_STAT_REQ: %s",
                      strerror(errno));
            error(Event::DriverError, "VIDIOC_OMAP3ISP_STAT_REQ: %s", strerror(errno));
            return Histogram();
        }

        Time h(hist_data.ts);
        h += 80000;
    }
    dprintf(3, "Ok we got a valid histogram\n");

    Histogram hist(64, 3, conf.region);
    for (int i = 0; i < 64; i++) {
        hist(i, 0) = buf[64 + i];  // r
        hist(i, 1) = buf[i];       // g
        hist(i, 2) = buf[128 + i]; // b
    }
    //hist.debug("Frame");
    return hist;
}

SharpnessMap V4L2Sensor::getSharpnessMap(Time t, const SharpnessMapConfig &conf) {
    if (!conf.enabled) {
        return SharpnessMap();
    }

    // grab the sharpness map for this frame
    struct omap3isp_stat_data af_data;
    unsigned buf[af.buf_size];

    af_data.buf = buf;
    af_data.buf_size = af.buf_size;
    af_data.frame_number = NEWEST_FRAME;
    //af_data.update = REQUEST_STATISTICS;
    af_data.cur_frame = 0;
    af_data.config_counter = 0;
    af_data.ts.tv_sec = 0;
    af_data.ts.tv_usec = 0;

    if (ioctl(affd, VIDIOC_OMAP3ISP_STAT_REQ, &af_data)) {
        if (errno != EBUSY) {
            error(Event::DriverError, "VIDIOC_OMAP3ISP_STAT_REQ: %s", strerror(errno));
        }
        return SharpnessMap();
    }

    Time s(af_data.ts);
    s += 80000;
    if ((t - s) > 4000) {
        warning(Event::DriverError, "Missing sharpness (%d)\n", t-s);
    }

    while ((t-s) < -4000) {
        // we got the wrong sharpness map
        if (af_data.frame_number == 0) { af_data.frame_number = 4095; }
        else { af_data.frame_number--; }

        if (ioctl(affd, VIDIOC_PRIVATE_ISP_AF_REQ, &af_data)) {
            if (errno != EBUSY) {
                error(Event::DriverError, "VIDIOC_PRIVATE_ISP_AF_REQ: %s", strerror(errno));
            }
            return SharpnessMap();
        }
        s = Time(af_data.ts);
        s += 80000;
    }

    SharpnessMap m(Size(16, 12), 3);
    unsigned *bufPtr = &buf[0];
    for (int y = 0; y < m.size().height; y++) {
        for (int x = 0; x < m.size().width; x++) {
            m(x, y, 0) = bufPtr[1];
            m(x, y, 1) = bufPtr[5];
            m(x, y, 2) = bufPtr[9];
            bufPtr += 12;
        }
    }

    return m;
}

void V4L2Sensor::setHistogramConfig(const HistogramConfig &histogram) {
    if (!histogram.enabled) { return; }

    // get the output size from the ccdc
    /*isp_pipeline_stats pstats;
    if (ioctl(fd, VIDIOC_PRIVATE_ISP_PIPELINE_STATS_REQ, &pstats) < 0) {
        error(Event::DriverError, "VIDIOC_PRIVATE_ISP_PIPELINE_STATS_REQ: %s", strerror(errno));
        return;
    }


    dprintf(4, "CCDC output: %d x %d\n", pstats.ccdc_out_w, pstats.ccdc_out_h);
    dprintf(4, "PRV  output: %d x %d\n", pstats.prv_out_w, pstats.prv_out_h);
    dprintf(4, "RSZ  input:  %d x %d + %d, %d\n",
            pstats.rsz_in_w, pstats.rsz_in_h,
            pstats.rsz_in_x, pstats.rsz_in_y);
    dprintf(4, "RSZ  output: %d x %d\n", pstats.rsz_out_w, pstats.rsz_out_h);
    */
    struct v4l2_subdev_format ccdc_out_fmt;
    memset(&ccdc_out_fmt, 0, sizeof(struct v4l2_subdev_format));

    // Hardcoded.. for now
    ccdc_out_fmt.pad = 1;
    ccdc_out_fmt.which = V4L2_SUBDEV_FORMAT_ACTIVE;

    if ( ioctl(ccdcfd, VIDIOC_SUBDEV_G_FMT, &ccdc_out_fmt) < 0) {
        dprintf(3,"Couldn't get CCDC output format\n");
        return;
    }

    //struct isp_hist_config hist_cfg;
    struct omap3isp_hist_config hist_cfg;
    //hist_cfg.enable = 1;
    //hist_cfg.source = HIST_SOURCE_CCDC;
    //hist_cfg.input_bit_width = 10;
    hist_cfg.buf_size = 64*4;
    hist_cfg.config_counter = 0;
    hist_cfg.num_acc_frames = 1;
    hist_cfg.hist_bins = OMAP3ISP_HIST_BINS_64;
    hist_cfg.cfa = OMAP3ISP_HIST_CFA_BAYER;
    // set the gains to slightly above 1 in 3Q5 format, in
    // order to use the full range in the histogram. Without
    // this, bucket #60 is saturated pixels, and 61-64 are
    // unused.
    hist_cfg.wg[0] = 35;
    hist_cfg.wg[1] = 35;
    hist_cfg.wg[2] = 35;
    hist_cfg.wg[3] = 35;
    hist_cfg.num_regions = 1;

    // set up its width and height
    unsigned x = ((unsigned)histogram.region.x * ccdc_out_fmt.format.width) / currentMode.width;
    unsigned y = ((unsigned)histogram.region.y * ccdc_out_fmt.format.height) / currentMode.height;
    unsigned w = ((unsigned)histogram.region.width * ccdc_out_fmt.format.width) / currentMode.width;
    unsigned h = ((unsigned)histogram.region.height * ccdc_out_fmt.format.height) / currentMode.height;
    if (x > ccdc_out_fmt.format.width) { x = ccdc_out_fmt.format.width-1; }
    if (y > ccdc_out_fmt.format.height) { y = ccdc_out_fmt.format.height-1; }
    if (w > ccdc_out_fmt.format.width) { w = ccdc_out_fmt.format.width-x; }
    if (h > ccdc_out_fmt.format.height) { h = ccdc_out_fmt.format.height-y; }
    //hist_cfg.reg_hor[0] = (x << 16) | w;
    //hist_cfg.reg_ver[0] = (y << 16) | h;
    hist_cfg.region[0].h_start = x;
    hist_cfg.region[0].h_end = x + w;
    hist_cfg.region[0].v_start = y;
    hist_cfg.region[0].v_end = y + h;
    dprintf(4, "Histogram size: %d x %d + %d, %d\n", w, h, x, y);

    dprintf(3, "Enabling histogram generator\n");
    // enable the histogram generator
    if (ioctl(histfd, VIDIOC_OMAP3ISP_HIST_CFG, &hist_cfg)) {
        error(Event::DriverError, "VIDIOC_OMAP3ISP_HIST_CFG: %s", strerror(errno));
        return;
    }
     
    // actual buffer size might have changed at this point, store the right one
    hist.buf_size = hist_cfg.buf_size;
    // store config counter so we can match it later
    hist.config_counter = hist_cfg.config_counter;
    dprintf(3, "Buf size %lu, config counter %lu\n", hist.buf_size, hist.config_counter);

    //Enable the stats module for histogram
    int en = 1;
    if (ioctl(histfd, VIDIOC_OMAP3ISP_STAT_EN, &en)) {
        error(Event::DriverError, "VIDIOC_OMAP3ISP_STAT_EN: %s", strerror(errno));
        return;
    }

    currentHistogram = histogram;
    currentHistogram.buckets = 64;
}

void V4L2Sensor::setSharpnessMapConfig(const SharpnessMapConfig &sharpness) {
    if (!sharpness.enabled) { return; }

    // Ignore the requested size and use 16x12
    Size size = Size(16, 12);

    // get the output size from the ccdc
    /*isp_pipeline_stats pstats;
    if (ioctl(fd, VIDIOC_PRIVATE_ISP_PIPELINE_STATS_REQ, &pstats) < 0) {
        error(Event::DriverError, "VIDIOC_PRIVATE_ISP_PIPELINE_STATS_REQ: %s", strerror(errno));
        return;
    }*/

    struct v4l2_subdev_format ccdc_out_fmt;
    memset(&ccdc_out_fmt, 0, sizeof(struct v4l2_subdev_format));

    // Hardcoded.. for now
    ccdc_out_fmt.pad = 1;
    ccdc_out_fmt.which = V4L2_SUBDEV_FORMAT_ACTIVE;

    if ( ioctl(ccdcfd, VIDIOC_SUBDEV_G_FMT, &ccdc_out_fmt) < 0) {
        dprintf(3,"Couldn't get CCDC output format\n");
        return;
    }

    struct omap3isp_h3a_af_config af_config;

    af_config.buf_size = 16*12*12;
    af_config.alaw_enable = 0;
    af_config.hmf.enable = 1;
    af_config.hmf.threshold = 10;
    af_config.rgb_pos = OMAP3ISP_AF_RG_GB_BAYER;
    af_config.iir.h_start = 0;

    // The IIR coefficients are as follows (yay reverse-engineering!)

    // The format is S6Q5 fixed point. A positive value of x
    // should be written as 32*x. A negatives value of x
    // should be written as 4096 - 32*x

    // 0: global gain? not sure.
    // 1-2: IIR taps on the first biquad
    // 3-5: FIR taps on the first biquad
    // 6-7: IIR taps on the second biquad
    // 8-10: FIR taps on the second biquad

    // A high pass filter aimed at ~8 pixel frequencies and above
    af_config.iir.coeff_set0[0] = 32; // gain of 1

    af_config.iir.coeff_set0[1] = 0; //4096-27;
    af_config.iir.coeff_set0[2] = 0; //6;

    af_config.iir.coeff_set0[3] = 16;
    af_config.iir.coeff_set0[4] = 4096-32;
    af_config.iir.coeff_set0[5] = 16;

    af_config.iir.coeff_set0[6] = 0;
    af_config.iir.coeff_set0[7] = 0;

    af_config.iir.coeff_set0[8] = 32;
    af_config.iir.coeff_set0[9] = 0;
    af_config.iir.coeff_set0[10] = 0;


    // A high pass filter aimed at ~4 pixel frequencies and above
    af_config.iir.coeff_set1[0] = 32; // gain of 1

    af_config.iir.coeff_set1[1] = 0;
    af_config.iir.coeff_set1[2] = 0;

    af_config.iir.coeff_set1[3] = 16;
    af_config.iir.coeff_set1[4] = 4096-32;
    af_config.iir.coeff_set1[5] = 16;

    af_config.iir.coeff_set1[6] = 0;
    af_config.iir.coeff_set1[7] = 0;

    af_config.iir.coeff_set1[8] = 32;
    af_config.iir.coeff_set1[9] = 0;
    af_config.iir.coeff_set1[10] = 0;

    af_config.fvmode = OMAP3ISP_AF_MODE_SUMMED;
    //af_config.af_config = H3A_AF_CFG_ENABLE;
    int paxWidth = ((ccdc_out_fmt.format.width-4) / (2*size.width))*2;
    int paxHeight = ((ccdc_out_fmt.format.height-4) / (2*size.height))*2;

    // These are internal errors because they should have been
    // caught and fixed earlier in the daemon class

    if (paxWidth > 256) {
        error(Event::InternalError, "AF paxels are too wide. Use a higher resolution sharpness map\n");
        return;
    }
    if (paxHeight > 256) {
        error(Event::InternalError, "AF paxels are too tall. Use a higher resolution sharpness map\n");
        return;
    }
    if (paxWidth < 16) {
        error(Event::InternalError, "AF paxels are too narrow. Use a lower resolution sharpness map\n");
        return;
    }
    if (paxHeight < 2) {
        error(Event::InternalError, "AF paxels are too short. Use a lower resolution sharpness map\n");
        return;
    }

    dprintf(4, "Using %d x %d paxels for af\n", paxWidth, paxHeight);
    af_config.paxel.width = paxWidth;
    af_config.paxel.height = paxHeight;
    af_config.paxel.h_start = (ccdc_out_fmt.format.width - size.width * paxWidth)/2;
    af_config.paxel.v_start = (ccdc_out_fmt.format.height - size.height * paxHeight)/2;
    af_config.paxel.h_cnt = size.width;
    af_config.paxel.v_cnt = size.height;
    af_config.paxel.line_inc = 2;

    dprintf(3, "Enabling sharpness detector\n");
    dprintf(3, "Requesting paxel : W: %d, H: %d, H_S: %d, V_S: %d, H_cnt:%d, V_cnt: %d\n",
            af_config.paxel.width,
            af_config.paxel.height,
            af_config.paxel.h_start,
            af_config.paxel.v_start,
            af_config.paxel.h_cnt,
            af_config.paxel.v_cnt);


    if (ioctl(affd, VIDIOC_OMAP3ISP_AF_CFG, &af_config)) {
        error(Event::DriverError, "VIDIOC_PRIVATE_ISP_AF_CFG: %s", strerror(errno));
        return;
    }
    // actual buffer size might have changed at this point, store the right one
    af.buf_size = af_config.buf_size;
    // store config counter so we can match it later
    af.config_counter = af_config.config_counter;
    dprintf(3, "AF buf size %lu, config counter %lu\n", hist.buf_size, hist.config_counter);
    
    //Enable the stats module for autoFocus
    int en = 1;
    if (ioctl(affd, VIDIOC_OMAP3ISP_STAT_EN, &en)) {
        error(Event::DriverError, "VIDIOC_OMAP3ISP_STAT_EN: %s", strerror(errno));
        return;
    }

    currentSharpness = sharpness;
}

void V4L2Sensor::releaseFrame(V4L2Frame *frame) {
    // requeue the buffer
    v4l2_buffer buf;
    memset(&buf, 0, sizeof(v4l2_buffer));
    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;
    buf.index = frame->index;

    if (ioctl(outputfd, VIDIOC_QBUF, &buf) < 0) {
        error(Event::DriverError, "VIDIOC_QBUF: %s", strerror(errno));
        return;
    }
}

void V4L2Sensor::setControl(unsigned int id, int value) {
    if (state == CLOSED) { return; }
    v4l2_control ctrl;
    ctrl.id = id;
    ctrl.value = value;
    if (ioctl(outputfd, VIDIOC_S_CTRL, &ctrl) < 0) {
        // TODO: Better error reporting for all the get/set
        error(Event::DriverError, "VIDIOC_S_CTRL: %s", strerror(errno));
        return;
    }
}

int V4L2Sensor::getControl(unsigned int id) {
    if (state == CLOSED) { return -1; }
    v4l2_control ctrl;
    ctrl.id = id;
    if (ioctl(outputfd, VIDIOC_G_CTRL, &ctrl) < 0) {
        error(Event::DriverError, "VIDIOC_G_CTRL: %s", strerror(errno));
        return -1;
    }

    return ctrl.value;
}

void V4L2Sensor::setExposure(int e) {
    if (state == CLOSED) { return; }

    dprintf(3, "Setting exposure to %d\n", e);

    struct v4l2_control ctrl;
    ctrl.id = V4L2_CID_EXPOSURE;
    ctrl.value = e;
    if (ioctl(sensorfd, VIDIOC_S_CTRL, &ctrl) < 0) {
        error(Event::DriverError, "VIDIOC_S_CTRL: %s", strerror(errno));
        return;
    }

}

int V4L2Sensor::getExposure() {
    if (state == CLOSED) { return -1; }

    struct v4l2_control ctrl;
    ctrl.id = V4L2_CID_EXPOSURE;

    if (ioctl(sensorfd, VIDIOC_G_CTRL, &ctrl) < 0) {
        error(Event::DriverError, "VIDIOC_G_CTRL: %s", strerror(errno));
        return -1;
    }

    dprintf(3, "Getting exposure: %d\n", ctrl.value);

    return ctrl.value;
}

#define V4L2_CID_FRAME_TIME (V4L2_CTRL_CLASS_CAMERA | 0x10ff)

void V4L2Sensor::setFrameTime(int e) {
    struct v4l2_subdev_frame_interval ival;
    int ret;

    if (state == CLOSED) { return; }

    memset(&ival, 0, sizeof(ival));

    // Sensor always has 1 pad only
    ival.pad = 0;
    if (e == 0) {
        ival.interval.numerator = 0;
        ival.interval.denominator = 1;
    } else {
        ival.interval.numerator = 1;
        ival.interval.denominator = (unsigned int)(1000000/e);
    }

    ret = ioctl(sensorfd, VIDIOC_SUBDEV_S_FRAME_INTERVAL, &ival);
    if (ret < 0)
        return;
    dprintf(3, "Frame time set to %d\n", e);
}

int V4L2Sensor::getFrameTime() {
    struct v4l2_subdev_frame_interval ival;
    int ret;

    if (state == CLOSED) { return -1; }

    memset(&ival, 0, sizeof(ival));

    // Sensor always has 1 pad only
    ival.pad = 0;

    ret = ioctl(sensorfd, VIDIOC_SUBDEV_G_FRAME_INTERVAL, &ival);
    if (ret < 0)
        return -1;

    dprintf(3, "Get frame time returning %lu/%lu = %lu\n", 
            ival.interval.numerator,
            ival.interval.denominator,
            (ival.interval.numerator*1000000)/ival.interval.denominator);
    return (int)((ival.interval.numerator*1000000)/ival.interval.denominator);
}

void V4L2Sensor::setGain(float g) {
    if (state == CLOSED) { return; }

    unsigned int gain;
    struct v4l2_control ctrl;

    dprintf(3, "Setting gain to %f\n", g);

    gain = (int)(g * 32.0 + 0.5);

    ctrl.id = V4L2_CID_GAIN;
    ctrl.value = gain;
    if (ioctl(sensorfd, VIDIOC_S_CTRL, &ctrl) < 0) {
        error(Event::DriverError, "VIDIOC_S_CTRL: %s", strerror(errno));
        return;
    }
}

float V4L2Sensor::getGain() {
    if (state == CLOSED) { return -1.0f; }

    struct v4l2_control ctrl;

    ctrl.id = V4L2_CID_GAIN;
    if (ioctl(sensorfd, VIDIOC_G_CTRL, &ctrl) < 0) {
        error(Event::DriverError, "VIDIOC_G_CTRL: %s", strerror(errno));
        return -1.0f;
    }

    return ctrl.value / 32.0f;
}

float max3(float a, float b, float c) {
    if (a > b && a > c) { return a; }
    else if (b > c) { return b; }
    return c;
}

void V4L2Sensor::setWhiteBalance(const float *matrix) {
    if (state == CLOSED) { return; }

    // Set the pre-demosiacing gains to one by default
    struct ispprv_update_config prvcfg;
    prvcfg.update = ISP_ABS_PREV_RGB2RGB | ISP_ABS_PREV_WB | ISP_ABS_PREV_BLKADJ;
    struct ispprev_wbal wbal;
    wbal.dgain = 256;
    wbal.coef0 = 32;
    wbal.coef1 = 32;
    wbal.coef2 = 32;
    wbal.coef3 = 32;
    prvcfg.prev_wbal = &wbal;

    // if any of the terms are particularly large, make use of the pre-demosaicing gains instead
    float rs = 1.0, gs = 1.0, bs = 1.0;
    float maxC = max3(fabs(matrix[0]), fabs(matrix[4]), fabs(matrix[8]));
    while (maxC > 8) {
        wbal.coef1 *= 2;
        maxC /= 2;
        rs *= 0.5;
    }

    maxC = max3(fabs(matrix[1]), fabs(matrix[5]), fabs(matrix[9]));
    while (maxC > 8) {
        wbal.coef0 *= 2;
        wbal.coef3 *= 2;
        maxC /= 2;
        gs *= 0.5;
    }

    maxC = max3(fabs(matrix[2]), fabs(matrix[6]), fabs(matrix[10]));
    while (maxC > 8) {
        wbal.coef2 *= 2;
        maxC /= 2;
        bs *= 0.5;
    }

    ispprev_rgbtorgb rgb2rgb;
    for (int i = 0; i < 3; i++) {
        rgb2rgb.matrix[i][0] = (signed short)(matrix[i*4+0]*rs*256);
        rgb2rgb.matrix[i][1] = (signed short)(matrix[i*4+1]*gs*256);
        rgb2rgb.matrix[i][2] = (signed short)(matrix[i*4+2]*bs*256);
    }
    rgb2rgb.offset[0] = (signed short)(matrix[3]);
    rgb2rgb.offset[1] = (signed short)(matrix[7]);
    rgb2rgb.offset[2] = (signed short)(matrix[11]);
    prvcfg.rgb2rgb = &rgb2rgb;

    // Similarly set the black level to zero (we take care of it in the matrix)
    struct ispprev_blkadj blkadj;
    blkadj.red = blkadj.green = blkadj.blue = 0;
    prvcfg.prev_blkadj = &blkadj;

    if (ioctl(fd, VIDIOC_PRIVATE_ISP_PRV_CFG, &prvcfg) < 0) {
        error(Event::DriverError, "VIDIOC_PRIVATE_ISP_PRV_CFG: %s", strerror(errno));
    }

}
}
}


