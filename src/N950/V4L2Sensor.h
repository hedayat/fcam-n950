#ifndef FCAM_V4L2_SENSOR_H
#define FCAM_V4L2_SENSOR_H

#include <vector>
#include <string>
#include <map>

#include <FCam/Base.h>
#include <FCam/Time.h>
#include <FCam/Histogram.h>
#include <FCam/SharpnessMap.h>

#include <linux/media.h>

namespace FCam {
namespace N950 {
// This class gives low-level control over the sensor using the
// V4L2 interface. It is used by the user-visible sensor object to
// control the sensor

class V4L2Sensor {
public:

    struct V4L2Frame {
        Time processingDoneTime;
        unsigned char *data;
        size_t length; // in bytes
        int index;
    };

    struct Mode {
        int width, height;
        ImageFormat type;
    };

    static V4L2Sensor *instance(std::string);

    // open and close the device
    void open();
    void close();

    // start and stop streaming
    void startStreaming(Mode,
                        const HistogramConfig &,
                        const SharpnessMapConfig &);
    void stopStreaming();

    V4L2Frame *acquireFrame(bool blocking);
    void releaseFrame(V4L2Frame *frame);

    Mode getMode() {return currentMode;}

    enum Errors {
        InvalidId,
        OutOfRangeValue,
        ControlBusy
    };

    void setControl(unsigned int id, int value);
    int getControl(unsigned int id);

    void setExposure(int);
    int getExposure();

    void setFrameTime(int);
    int getFrameTime();

    void setGain(float);
    float getGain();

    void setWhiteBalance(const float *);

    Histogram getHistogram(Time t, const HistogramConfig &conf);
    void setHistogramConfig(const HistogramConfig &);

    SharpnessMap getSharpnessMap(Time t, const SharpnessMapConfig &conf);
    void setSharpnessMapConfig(const SharpnessMapConfig &);

    int getFD();
    int getCCDCFd();

private:
    //std::string rawPipeline = "jt8ev1|OMAP3 ISP CSI2a|OMAP3 ISP CCDC|OMAP3 ISP CCDC output";

    struct Entity {
        struct media_entity_desc info;
        std::map<int,struct media_pad_desc> pads;
        std::map<int,struct media_link_desc> links;
        std::string devname;
        int fd;
    };

    struct Stat {
             __u32 buf_size;
             __u16 config_counter;

    };

    int frameTime;

    V4L2Sensor(std::string);
    std::string pathFromEntity(struct Entity e);
    void enumerateEntities();
    bool setupPipeline(std::string);
    void enumerateSensorFormats();

    Mode currentMode;
    SharpnessMapConfig currentSharpness;
    HistogramConfig currentHistogram;

    std::vector<V4L2Frame> buffers;

    enum {CLOSED=0, IDLE, STREAMING} state;
    int fd;
    std::string sensorPath;
    std::string flashPath;
    std::string lensPath;
    int sensorfd;
    int lensfd;
    int flashfd;
    int outputfd;
    int ccdcfd;
    int histfd;
    int affd;
    std::map<int, Entity> entities;
    struct Stat hist;
    struct Stat af;

    std::string filename;

    // there is one of these objects per device
    static std::map<std::string, V4L2Sensor *> instances_;
};
}
}

#endif

