#include "FCam/FCam.h"
#include "FCam/Dummy.h"
#include <stdio.h>
#include <math.h>

using namespace FCam;

#ifdef FCAM_ARCH_ARM

// Returns time in us for reading a 16-bit image of some size, using NEON assembly
float testMemoryRWASM(unsigned int width, unsigned int height) {
    const int iterations = 10;
    unsigned int totalT = 0;
    width = width / 16 * 16; // Round down to nearest multiple of 16
    height = height / 4 * 4; // Round down to nearest multiple of 4

    for (int n=0; n < iterations; n++) {
        uint16_t *mem = new uint16_t[width*height];
        uint8_t *dst = new uint8_t[width*height/16*3];
        if (!mem) {
            printf("Unable to allocate %d bytes, bailing\n", 2*width*height);
            return -1;
        }
        if (!dst) {
            printf("Unable to allocate %d bytes, bailing\n", width*height/16*3);
            return -1;
        }
        memset(mem, 255, 2*width*height);
        memset(dst, 0, width*height/16*3);
        uint16_t *row = mem;
        uint8_t *dstrow = dst;
        Time startT = Time::now();

        for (unsigned int y = 0; y < height; y+=4, row += 4*width) {
            register unsigned short *px0 = (unsigned short *)row;
            register unsigned short *px1 = (unsigned short *)(row+1*width);
            register unsigned short *px2 = (unsigned short *)(row+2*width);
            register unsigned short *px3 = (unsigned short *)(row+3*width);

            for (register unsigned int x =0; x < width; x+=16) {
                // Assembly loading block based on thumbnail code
                // Only contains data move, no arithmetic - maximum speed possible given current thumbnailing design
                asm volatile(
                    //// Load a 16x4 block of 16-bit pixels from memory, de-interleave by 2 horizontally
                    "vld2.16 {d4-d7}, [%[px0]]!  \n\t"
                    "vld2.16 {d8-d11}, [%[px1]]! \n\t"
                    "vld2.16 {d12-d15}, [%[px2]]! \n\t"
                    "vld2.16 {d16-d19}, [%[px3]]! \n\t"
                    //// Move some of this to CPU side
                    "vmov r5,r6,d4  \n\t"
                    "vmov r4,r5,d8  \n\t"
                    "vmov r7,r8,d16  \n\t"
                    //// Write 4x1 RGB pixels back as uint8 from CPU
                    "stm %[dstrow]!, {r5,r6,r7}"
                    : [px0] "+&r"(px0),
                    [px1] "+&r"(px1),
                    [px2] "+&r"(px2),
                    [px3] "+&r"(px3),
                    [dstrow] "+&r"(dstrow)
                    :
                    : "memory",
                    "r4", "r5", "r6", "r7", "r8",
                    "d4", "d5", "d6",
                    "d7", "d8", "d9", "d10",
                    "d11", "d12", "d13", "d14",
                    "d15", "d16", "d17", "d18", "d19"
                );
            }
        }
        Time endT = Time::now();
        //printf("%f %d %d #", (endT-startT)/1000.f, dst[0], dst[width*height/16]);
        totalT += endT-startT;
        delete mem;
        delete dst;
    }
    //printf("\n totalT: %d %d", totalT, iterations);
    return totalT/iterations;

}

#endif

// Returns time in us for reading a 16-bit image of some size
float testMemoryRSimple(unsigned int width, unsigned int height) {
    const int iterations = 10;
    unsigned int totalT = 0;
    for (int n=0; n < iterations; n++) {
        uint16_t *mem = new uint16_t[width*height];
        if (!mem) {
            printf("Unable to allocate %d bytes, bailing\n", 2*width*height);
            return -1;
        }
        memset(mem, 0, 2*width*height);
        Time startT = Time::now();
        unsigned int loc =0;
        for (unsigned int y=0; y < height; y++) {
            for (unsigned int x=0; x < width; x++) {
                if (mem[loc] == 1) {
                    printf("never gonna happen\n");
                }
                loc++;
            }
        }
        Time endT = Time::now();
        //printf("%f ", (endT-startT)/1000.f);
        totalT += endT-startT;
        delete mem;
    }
    //printf("\n");
    return (float)totalT/iterations;
}


// Returns time in us for reading and writing a 16-bit image of some size
float testMemoryRWSimple(unsigned int width, unsigned int height) {
    const int iterations = 10;
    unsigned int totalT = 0;
    for (int n=0; n < iterations; n++) {
        uint16_t *mem = new uint16_t[width*height];
        if (!mem) {
            printf("Unable to allocate %d bytes, bailing\n", 2*width*height);
            return -1;
        }
        memset(mem, 0, 2*width*height);
        Time startT = Time::now();
        unsigned int loc =0;
        for (unsigned int y=0; y < height; y++) {
            for (unsigned int x=0; x < width; x++) {
                mem[loc] += 1;
                loc++;
            }
        }
        Time endT = Time::now();
        //printf("%d ", (endT-startT));
        totalT += endT-startT;
        delete mem;
    }
    //printf("\n");
    return (float)totalT/iterations;
}

int main(int argc, const char **argv) {

    Dummy::_Frame *_f = new Dummy::_Frame;
    Dummy::Frame f(_f);

    printf("** Testing demosaic results on a white jaggy circle (output to circle.tmp)\n");
    Image circle(128, 128, RAW);
    short *data = (short *)circle(0,0);
    for (int y = 0; y < 128; y++) {
        for (int x = 0; x < 128; x++) {
            bool in = ((x-64)*(x-64) + (y-64)*(y-64)) < 2500;
            *data++ = in ? 1023 : 0;
        }
    }

    _f->image = circle;
    FCAM_IMAGE_DEBUG(f.image());
    saveDump(demosaic(f), "circle.tmp");

    printf("Testing demosaic results on a white anti-aliased circle\n   (input: circle_in.tmp, output: circle_out.tmp)\n");
    data = (short *)circle(0,0);
    for (int y = 0; y < 128; y++) {
        for (int x = 0; x < 128; x++) {
            float d = sqrtf(((x-64)*(x-64) + (y-64)*(y-64)));
            *data++ = (d > 50 ? 0 : (d < 49 ? 1023 : (50 - d)*1023));
        }
    }

    saveDump(circle, "circle_in.tmp");
    saveDump(demosaic(f), "circle_out.tmp");

    printf("** Testing demosaic on a subimage\n   (input: circle_canvas_in.tmp, subimage: circle_sub_in.tmp, output:circle_sub.tmp)\n");
    Image canvas(200,200, RAW);
    for (int y=0; y < 200; y++) {
        for (int x=0; x < 200; x+=2) {
            ((short *)canvas(x,y))[0] = 0;
            ((short *)canvas(x,y))[1] = 1023;
        }
    }
    Image subCanvas = canvas.subImage(36,36,Size(128,128));
    subCanvas.copyFrom(circle);
    FCAM_IMAGE_DEBUG(canvas);
    FCAM_IMAGE_DEBUG(subCanvas);
    saveDump(canvas, "circle_canvas_in.tmp");
    saveDump(subCanvas, "circle_sub_in.tmp");
    saveDump(demosaic(f), "circle_sub.tmp");

    printf("** Testing demosaic speed - 2560x1920\n");

    Image in(64*40, 48*40, RAW);

    _f->image = in;

    Time t1 = Time::now();

    for (int i = 0; i < 4; i++) {
        demosaic(f);
    }

    printf("Demosaic average duration (4 runs): %d ms\n", (Time::now() - t1)/4000);

    printf("** Testing thumbnail generation (input: thumb_in.tmp, output: thumb_out.tmp)\n");

    Image in2(2592,1968, RAW);
    for (unsigned int y=0; y < in2.height()-1; y+=2) {
        for (unsigned int x=0; x < in2.width()-1; x+=2) {
            *((short *)in2(x,y))   = 500 * ((x / 50) % 2) * ((y / 50) % 2); // G
            *((short *)in2(x+1,y)) = 500 * ((x / 25) % 2) * ((y / 25) % 2); // R
            *((short *)in2(x,y+1)) = 500 * ((x / 100) % 2) * ((y / 100) % 2); // B
            *((short *)in2(x+1,y+1))   = 500 * ((x / 50) % 2) * ((y / 50) % 2); // G
        }
    }
    saveDump(in2, "thumb_in.tmp");
    _f->image = in2;
    saveDump(makeThumbnail(f), "thumb_out.tmp");

#ifdef FCAM_ARCH_ARM
    printf("** Testing thumbnail speed, N900-asm 2592x1968 GRBG -> 640x480 \n");

    _f->image = in2;
    Time t3 = Time::now();
    for (int i=0; i<10; i++) {
        makeThumbnail(f);
    }
    printf("Thumbnailing average duration (10 runs): %d ms\n", (Time::now() - t3)/10000);
#endif

    printf("** Testing thumbnail speed, generic 2591x1967 GRBG -> 640x480 \n");
    Image in3(2591,1967, RAW);
    _f->image = in3;
    Time t2 = Time::now();
    for (int i=0; i<10; i++) {
        makeThumbnail(f);
    }
    printf("Thumbnailing average duration (10 runs): %d ms\n", (Time::now() - t2)/10000);

    printf("** Simple memory bandwidth tests\n");
    float memTime = testMemoryRSimple(2592,1968);
    printf("Average time to read a 2592x1968 memory buffer: %f ms\n", memTime/1000);
    memTime = testMemoryRWSimple(2592,1968);
    printf("Average time to read/write a 2592x1968 memory buffer: %f ms\n", memTime/1000);

#ifdef FCAM_ARCH_ARM
    memTime = testMemoryRWASM(2592,1968);
    printf("Average time to read a 2592x1968 memory buffer, and write a 648x492 output, ARM NEON: %f ms\n", memTime/1000);
#endif

    return 0;
};
