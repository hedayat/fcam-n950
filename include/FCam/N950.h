#ifndef FCAM_N950_H
#define FCAM_N950_H

/** \file
 * Including this file includes all the necessary components for the Nokia N950 implementation */

#include "FCam.h"

#include "N950/Sensor.h"
//#include "N950/Flash.h"
#include "N950/Lens.h"
#include "N950/Frame.h"
#include "N950/Platform.h"

namespace FCam {
/** The namespace for the N950 platform */
namespace N950 {
}
}

#endif
