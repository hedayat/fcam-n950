#ifndef FCAM_N950_FRAME_H
#define FCAM_N950_FRAME_H

/** \file
 * The N950 Frame class.
 */

#include "../Frame.h"
#include "Platform.h"

namespace FCam {
namespace N950 {

struct _Frame : public FCam::_Frame {

    FCam::Shot _shot;
    const FCam::Shot &shot() const { return _shot; }
    const FCam::Shot &baseShot() const { return shot(); }

    const FCam::Platform &platform() const {return ::FCam::N950::Platform::instance();}
};


/** The N950 Frame class. It currently adds no features to the
    base frame class, apart from implementing the required
    virtual methods. This may change in the future - there are
    some unexploited features of the N950 sensor (notably,
    digital zoom). */
class Frame : public FCam::Frame {
public:
    Frame(_Frame *f=NULL) : FCam::Frame(f) {}
    ~Frame();
};
}
}

#endif
