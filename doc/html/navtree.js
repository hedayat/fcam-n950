var NAVTREE =
[
  [ "FCam", "index.html", [
    [ "FCam: An API for controlling computational cameras.", "index.html", null ],
    [ "Related Pages", "pages.html", [
      [ "Examples", "_examples.html", null ],
      [ "Todo List", "todo.html", null ]
    ] ],
    [ "Class List", "annotated.html", [
      [ "FCam::_DNGFrame", "class_f_cam_1_1___d_n_g_frame.html", null ],
      [ "FCam::_Frame", "struct_f_cam_1_1___frame.html", null ],
      [ "FCam::Action", "class_f_cam_1_1_action.html", null ],
      [ "FCam::AsyncFileWriter", "class_f_cam_1_1_async_file_writer.html", null ],
      [ "FCam::AutoFocus", "class_f_cam_1_1_auto_focus.html", null ],
      [ "FCam::CopyableAction< Derived >", "class_f_cam_1_1_copyable_action.html", null ],
      [ "FCam::Device", "class_f_cam_1_1_device.html", null ],
      [ "FCam::DNGFrame", "class_f_cam_1_1_d_n_g_frame.html", null ],
      [ "FCam::Dummy::Platform", "class_f_cam_1_1_dummy_1_1_platform.html", null ],
      [ "FCam::Dummy::Sensor", "class_f_cam_1_1_dummy_1_1_sensor.html", null ],
      [ "FCam::Dummy::Shot", "class_f_cam_1_1_dummy_1_1_shot.html", null ],
      [ "FCam::Event", "class_f_cam_1_1_event.html", null ],
      [ "FCam::EventGenerator", "class_f_cam_1_1_event_generator.html", null ],
      [ "FCam::F2::_Frame", "struct_f_cam_1_1_f2_1_1___frame.html", null ],
      [ "FCam::F2::Flash", "class_f_cam_1_1_f2_1_1_flash.html", null ],
      [ "FCam::F2::Flash::StrobeStartAction", "class_f_cam_1_1_f2_1_1_flash_1_1_strobe_start_action.html", null ],
      [ "FCam::F2::Flash::StrobeStopAction", "class_f_cam_1_1_f2_1_1_flash_1_1_strobe_stop_action.html", null ],
      [ "FCam::F2::Frame", "class_f_cam_1_1_f2_1_1_frame.html", null ],
      [ "FCam::F2::Lens", "class_f_cam_1_1_f2_1_1_lens.html", null ],
      [ "FCam::F2::PhidgetDevice", "class_f_cam_1_1_f2_1_1_phidget_device.html", null ],
      [ "FCam::F2::PhidgetFlash", "class_f_cam_1_1_f2_1_1_phidget_flash.html", null ],
      [ "FCam::F2::Platform", "class_f_cam_1_1_f2_1_1_platform.html", null ],
      [ "FCam::F2::Sensor", "class_f_cam_1_1_f2_1_1_sensor.html", null ],
      [ "FCam::F2::Shot", "class_f_cam_1_1_f2_1_1_shot.html", null ],
      [ "FCam::F2::ShutterButton", "class_f_cam_1_1_f2_1_1_shutter_button.html", null ],
      [ "FCam::Flash", "class_f_cam_1_1_flash.html", null ],
      [ "FCam::Flash::FireAction", "class_f_cam_1_1_flash_1_1_fire_action.html", null ],
      [ "FCam::Flash::Tags", "class_f_cam_1_1_flash_1_1_tags.html", null ],
      [ "FCam::Frame", "class_f_cam_1_1_frame.html", null ],
      [ "FCam::Histogram", "class_f_cam_1_1_histogram.html", null ],
      [ "FCam::HistogramConfig", "class_f_cam_1_1_histogram_config.html", null ],
      [ "FCam::Image", "class_f_cam_1_1_image.html", null ],
      [ "FCam::Lens", "class_f_cam_1_1_lens.html", null ],
      [ "FCam::Lens::ApertureAction", "class_f_cam_1_1_lens_1_1_aperture_action.html", null ],
      [ "FCam::Lens::FocusAction", "class_f_cam_1_1_lens_1_1_focus_action.html", null ],
      [ "FCam::Lens::Tags", "struct_f_cam_1_1_lens_1_1_tags.html", null ],
      [ "FCam::Lens::ZoomAction", "class_f_cam_1_1_lens_1_1_zoom_action.html", null ],
      [ "FCam::N900::Flash", "class_f_cam_1_1_n900_1_1_flash.html", null ],
      [ "FCam::N900::Frame", "class_f_cam_1_1_n900_1_1_frame.html", null ],
      [ "FCam::N900::Lens", "class_f_cam_1_1_n900_1_1_lens.html", null ],
      [ "FCam::N900::Platform", "class_f_cam_1_1_n900_1_1_platform.html", null ],
      [ "FCam::N900::Sensor", "class_f_cam_1_1_n900_1_1_sensor.html", null ],
      [ "FCam::Platform", "class_f_cam_1_1_platform.html", null ],
      [ "FCam::Rect", "struct_f_cam_1_1_rect.html", null ],
      [ "FCam::Sensor", "class_f_cam_1_1_sensor.html", null ],
      [ "FCam::SharpnessMap", "class_f_cam_1_1_sharpness_map.html", null ],
      [ "FCam::SharpnessMapConfig", "class_f_cam_1_1_sharpness_map_config.html", null ],
      [ "FCam::Shot", "class_f_cam_1_1_shot.html", null ],
      [ "FCam::Size", "struct_f_cam_1_1_size.html", null ],
      [ "FCam::TagValue", "class_f_cam_1_1_tag_value.html", null ],
      [ "FCam::Time", "class_f_cam_1_1_time.html", null ],
      [ "FCam::TSQueue< T >", "class_f_cam_1_1_t_s_queue.html", null ]
    ] ],
    [ "Class Index", "classes.html", null ],
    [ "Class Hierarchy", "hierarchy.html", [
      [ "FCam::Action", "class_f_cam_1_1_action.html", [
        [ "FCam::CopyableAction< ApertureAction >", "class_f_cam_1_1_copyable_action.html", [
          [ "FCam::Lens::ApertureAction", "class_f_cam_1_1_lens_1_1_aperture_action.html", null ]
        ] ],
        [ "FCam::CopyableAction< FireAction >", "class_f_cam_1_1_copyable_action.html", [
          [ "FCam::Flash::FireAction", "class_f_cam_1_1_flash_1_1_fire_action.html", null ]
        ] ],
        [ "FCam::CopyableAction< FocusAction >", "class_f_cam_1_1_copyable_action.html", [
          [ "FCam::Lens::FocusAction", "class_f_cam_1_1_lens_1_1_focus_action.html", null ]
        ] ],
        [ "FCam::CopyableAction< SoundAction >", "class_f_cam_1_1_copyable_action.html", null ],
        [ "FCam::CopyableAction< StrobeStartAction >", "class_f_cam_1_1_copyable_action.html", [
          [ "FCam::F2::Flash::StrobeStartAction", "class_f_cam_1_1_f2_1_1_flash_1_1_strobe_start_action.html", null ]
        ] ],
        [ "FCam::CopyableAction< StrobeStopAction >", "class_f_cam_1_1_copyable_action.html", [
          [ "FCam::F2::Flash::StrobeStopAction", "class_f_cam_1_1_f2_1_1_flash_1_1_strobe_stop_action.html", null ]
        ] ],
        [ "FCam::CopyableAction< ZoomAction >", "class_f_cam_1_1_copyable_action.html", [
          [ "FCam::Lens::ZoomAction", "class_f_cam_1_1_lens_1_1_zoom_action.html", null ]
        ] ],
        [ "FCam::CopyableAction< Derived >", "class_f_cam_1_1_copyable_action.html", null ]
      ] ],
      [ "FCam::AsyncFileWriter", "class_f_cam_1_1_async_file_writer.html", null ],
      [ "FCam::AutoFocus", "class_f_cam_1_1_auto_focus.html", null ],
      [ "FCam::Event", "class_f_cam_1_1_event.html", null ],
      [ "FCam::EventGenerator", "class_f_cam_1_1_event_generator.html", [
        [ "FCam::_Frame", "struct_f_cam_1_1___frame.html", [
          [ "FCam::_DNGFrame", "class_f_cam_1_1___d_n_g_frame.html", null ],
          [ "FCam::F2::_Frame", "struct_f_cam_1_1_f2_1_1___frame.html", null ]
        ] ],
        [ "FCam::Device", "class_f_cam_1_1_device.html", [
          [ "FCam::F2::PhidgetDevice", "class_f_cam_1_1_f2_1_1_phidget_device.html", [
            [ "FCam::F2::PhidgetFlash", "class_f_cam_1_1_f2_1_1_phidget_flash.html", null ]
          ] ],
          [ "FCam::Flash", "class_f_cam_1_1_flash.html", [
            [ "FCam::F2::Flash", "class_f_cam_1_1_f2_1_1_flash.html", null ],
            [ "FCam::N900::Flash", "class_f_cam_1_1_n900_1_1_flash.html", null ]
          ] ],
          [ "FCam::Lens", "class_f_cam_1_1_lens.html", [
            [ "FCam::F2::Lens", "class_f_cam_1_1_f2_1_1_lens.html", null ],
            [ "FCam::N900::Lens", "class_f_cam_1_1_n900_1_1_lens.html", null ]
          ] ],
          [ "FCam::Sensor", "class_f_cam_1_1_sensor.html", [
            [ "FCam::Dummy::Sensor", "class_f_cam_1_1_dummy_1_1_sensor.html", null ],
            [ "FCam::F2::Sensor", "class_f_cam_1_1_f2_1_1_sensor.html", null ],
            [ "FCam::N900::Sensor", "class_f_cam_1_1_n900_1_1_sensor.html", null ]
          ] ]
        ] ],
        [ "FCam::F2::ShutterButton", "class_f_cam_1_1_f2_1_1_shutter_button.html", null ]
      ] ],
      [ "FCam::Flash::Tags", "class_f_cam_1_1_flash_1_1_tags.html", null ],
      [ "FCam::Frame", "class_f_cam_1_1_frame.html", [
        [ "FCam::DNGFrame", "class_f_cam_1_1_d_n_g_frame.html", null ],
        [ "FCam::F2::Frame", "class_f_cam_1_1_f2_1_1_frame.html", null ],
        [ "FCam::N900::Frame", "class_f_cam_1_1_n900_1_1_frame.html", null ]
      ] ],
      [ "FCam::Histogram", "class_f_cam_1_1_histogram.html", null ],
      [ "FCam::HistogramConfig", "class_f_cam_1_1_histogram_config.html", null ],
      [ "FCam::Image", "class_f_cam_1_1_image.html", null ],
      [ "FCam::Lens::Tags", "struct_f_cam_1_1_lens_1_1_tags.html", null ],
      [ "FCam::Platform", "class_f_cam_1_1_platform.html", [
        [ "FCam::_DNGFrame", "class_f_cam_1_1___d_n_g_frame.html", null ],
        [ "FCam::Dummy::Platform", "class_f_cam_1_1_dummy_1_1_platform.html", null ],
        [ "FCam::F2::Platform", "class_f_cam_1_1_f2_1_1_platform.html", null ],
        [ "FCam::N900::Platform", "class_f_cam_1_1_n900_1_1_platform.html", null ]
      ] ],
      [ "FCam::Rect", "struct_f_cam_1_1_rect.html", null ],
      [ "FCam::SharpnessMap", "class_f_cam_1_1_sharpness_map.html", null ],
      [ "FCam::SharpnessMapConfig", "class_f_cam_1_1_sharpness_map_config.html", null ],
      [ "FCam::Shot", "class_f_cam_1_1_shot.html", [
        [ "FCam::Dummy::Shot", "class_f_cam_1_1_dummy_1_1_shot.html", null ],
        [ "FCam::F2::Shot", "class_f_cam_1_1_f2_1_1_shot.html", null ]
      ] ],
      [ "FCam::Size", "struct_f_cam_1_1_size.html", null ],
      [ "FCam::TagValue", "class_f_cam_1_1_tag_value.html", null ],
      [ "FCam::Time", "class_f_cam_1_1_time.html", null ],
      [ "FCam::TSQueue< T >", "class_f_cam_1_1_t_s_queue.html", null ]
    ] ],
    [ "Class Members", "functions.html", null ],
    [ "Namespace List", "namespaces.html", [
      [ "FCam", "namespace_f_cam.html", null ],
      [ "FCam::Dummy", "namespace_f_cam_1_1_dummy.html", null ],
      [ "FCam::F2", "namespace_f_cam_1_1_f2.html", null ],
      [ "FCam::F2::ColBin", "namespace_f_cam_1_1_f2_1_1_col_bin.html", null ],
      [ "FCam::F2::ColSkip", "namespace_f_cam_1_1_f2_1_1_col_skip.html", null ],
      [ "FCam::F2::RowBin", "namespace_f_cam_1_1_f2_1_1_row_bin.html", null ],
      [ "FCam::F2::RowSkip", "namespace_f_cam_1_1_f2_1_1_row_skip.html", null ],
      [ "FCam::N900", "namespace_f_cam_1_1_n900.html", null ]
    ] ],
    [ "Namespace Members", "namespacemembers.html", null ],
    [ "File List", "files.html", [
      [ "gain_ramp.cpp", "gain__ramp_8cpp.html", null ],
      [ "examples/example1/example1.cpp", "example1_8cpp.html", null ],
      [ "examples/example2/example2.cpp", "example2_8cpp.html", null ],
      [ "examples/example3/example3.cpp", "example3_8cpp.html", null ],
      [ "examples/example4/example4.cpp", "example4_8cpp.html", null ],
      [ "examples/example5/example5.cpp", "example5_8cpp.html", null ],
      [ "examples/example6/example6.cpp", "example6_8cpp.html", null ],
      [ "examples/example6/SoundPlayer.cpp", "_sound_player_8cpp.html", null ],
      [ "examples/example6/SoundPlayer.h", "_sound_player_8h.html", null ],
      [ "examples/example7/CameraThread.cpp", "_camera_thread_8cpp.html", null ],
      [ "examples/example7/CameraThread.h", "_camera_thread_8h.html", null ],
      [ "examples/example7/example7.cpp", "example7_8cpp.html", null ],
      [ "examples/example7/omapfb.h", null, null ],
      [ "examples/example7/OverlayWidget.cpp", null, null ],
      [ "examples/example7/OverlayWidget.h", null, null ],
      [ "include/FCam/Action.h", "_action_8h.html", null ],
      [ "include/FCam/AsyncFile.h", "_async_file_8h.html", null ],
      [ "include/FCam/AutoExposure.h", "_auto_exposure_8h.html", null ],
      [ "include/FCam/AutoFocus.h", "_auto_focus_8h.html", null ],
      [ "include/FCam/AutoWhiteBalance.h", "_auto_white_balance_8h.html", null ],
      [ "include/FCam/Base.h", "_base_8h.html", null ],
      [ "include/FCam/CircularBuffer.h", null, null ],
      [ "include/FCam/Device.h", "_device_8h.html", null ],
      [ "include/FCam/Dummy.h", "_dummy_8h.html", null ],
      [ "include/FCam/Event.h", "_event_8h.html", null ],
      [ "include/FCam/F2.h", "_f2_8h.html", null ],
      [ "include/FCam/FCam.h", "_f_cam_8h.html", null ],
      [ "include/FCam/Flash.h", "_flash_8h.html", null ],
      [ "include/FCam/Frame.h", "_frame_8h.html", null ],
      [ "include/FCam/Histogram.h", "_histogram_8h.html", null ],
      [ "include/FCam/Image.h", "_image_8h.html", null ],
      [ "include/FCam/Lens.h", "_lens_8h.html", null ],
      [ "include/FCam/N900.h", "_n900_8h.html", null ],
      [ "include/FCam/Platform.h", "_platform_8h.html", null ],
      [ "include/FCam/Sensor.h", "_sensor_8h.html", null ],
      [ "include/FCam/SharpnessMap.h", "_sharpness_map_8h.html", null ],
      [ "include/FCam/Shot.h", "_shot_8h.html", null ],
      [ "include/FCam/TagValue.h", "_tag_value_8h.html", null ],
      [ "include/FCam/Time.h", "_time_8h.html", null ],
      [ "include/FCam/TSQueue.h", "_t_s_queue_8h.html", null ],
      [ "include/FCam/Dummy/Frame.h", null, null ],
      [ "include/FCam/Dummy/Platform.h", "_dummy_2_platform_8h.html", null ],
      [ "include/FCam/Dummy/Sensor.h", "_dummy_2_sensor_8h.html", null ],
      [ "include/FCam/Dummy/Shot.h", "_dummy_2_shot_8h.html", null ],
      [ "include/FCam/F2/EF232LensDatabase.h", null, null ],
      [ "include/FCam/F2/Flash.h", "_f2_2_flash_8h.html", null ],
      [ "include/FCam/F2/Frame.h", "_f2_2_frame_8h.html", null ],
      [ "include/FCam/F2/Lens.h", "_f2_2_lens_8h.html", null ],
      [ "include/FCam/F2/Platform.h", "_f2_2_platform_8h.html", null ],
      [ "include/FCam/F2/Sensor.h", "_f2_2_sensor_8h.html", null ],
      [ "include/FCam/F2/Shot.h", "_f2_2_shot_8h.html", null ],
      [ "include/FCam/F2/ShutterButton.h", "_shutter_button_8h.html", null ],
      [ "include/FCam/N900/Flash.h", "_n900_2_flash_8h.html", null ],
      [ "include/FCam/N900/Frame.h", "_n900_2_frame_8h.html", null ],
      [ "include/FCam/N900/Lens.h", "_n900_2_lens_8h.html", null ],
      [ "include/FCam/N900/Platform.h", "_n900_2_platform_8h.html", null ],
      [ "include/FCam/N900/Sensor.h", "_n900_2_sensor_8h.html", null ],
      [ "include/FCam/processing/Color.h", "_color_8h.html", null ],
      [ "include/FCam/processing/Demosaic.h", "_demosaic_8h.html", null ],
      [ "include/FCam/processing/DNG.h", "_d_n_g_8h.html", null ],
      [ "include/FCam/processing/Dump.h", "_dump_8h.html", null ],
      [ "include/FCam/processing/JPEG.h", "_j_p_e_g_8h.html", null ],
      [ "src/Action.cpp", null, null ],
      [ "src/AsyncFile.cpp", null, null ],
      [ "src/AutoExposure.cpp", null, null ],
      [ "src/AutoFocus.cpp", null, null ],
      [ "src/AutoWhiteBalance.cpp", null, null ],
      [ "src/Base.cpp", null, null ],
      [ "src/Debug.h", "_debug_8h.html", null ],
      [ "src/Device.cpp", null, null ],
      [ "src/Event.cpp", null, null ],
      [ "src/Flash.cpp", null, null ],
      [ "src/Frame.cpp", null, null ],
      [ "src/Histogram.cpp", null, null ],
      [ "src/Image.cpp", null, null ],
      [ "src/Lens.cpp", null, null ],
      [ "src/Sensor.cpp", null, null ],
      [ "src/Shot.cpp", null, null ],
      [ "src/TagValue.cpp", null, null ],
      [ "src/Time.cpp", null, null ],
      [ "src/Dummy/Daemon.cpp", null, null ],
      [ "src/Dummy/Daemon.h", null, null ],
      [ "src/Dummy/Frame.cpp", null, null ],
      [ "src/Dummy/Platform.cpp", null, null ],
      [ "src/Dummy/Sensor.cpp", null, null ],
      [ "src/Dummy/Shot.cpp", null, null ],
      [ "src/F2/Daemon.cpp", null, null ],
      [ "src/F2/Daemon.h", null, null ],
      [ "src/F2/EF232LensDatabase.cpp", null, null ],
      [ "src/F2/Flash.cpp", null, null ],
      [ "src/F2/Frame.cpp", null, null ],
      [ "src/F2/Lens.cpp", null, null ],
      [ "src/F2/PhidgetDevice.cpp", null, null ],
      [ "src/F2/PhidgetDevice.h", "_phidget_device_8h.html", null ],
      [ "src/F2/Platform.cpp", null, null ],
      [ "src/F2/Sensor.cpp", null, null ],
      [ "src/F2/Shot.cpp", null, null ],
      [ "src/F2/ShutterButton.cpp", null, null ],
      [ "src/F2/V4L2Sensor.cpp", null, null ],
      [ "src/F2/V4L2Sensor.h", null, null ],
      [ "src/N900/ButtonListener.cpp", null, null ],
      [ "src/N900/ButtonListener.h", null, null ],
      [ "src/N900/Daemon.cpp", null, null ],
      [ "src/N900/Daemon.h", null, null ],
      [ "src/N900/Flash.cpp", null, null ],
      [ "src/N900/Frame.cpp", null, null ],
      [ "src/N900/Lens.cpp", null, null ],
      [ "src/N900/Platform.cpp", null, null ],
      [ "src/N900/Sensor.cpp", null, null ],
      [ "src/N900/V4L2Sensor.cpp", null, null ],
      [ "src/N900/V4L2Sensor.h", null, null ],
      [ "src/processing/Color.cpp", null, null ],
      [ "src/processing/Demosaic.cpp", null, null ],
      [ "src/processing/Demosaic_ARM.cpp", null, null ],
      [ "src/processing/Demosaic_ARM.h", null, null ],
      [ "src/processing/DNG.cpp", null, null ],
      [ "src/processing/Dump.cpp", null, null ],
      [ "src/processing/JPEG.cpp", null, null ],
      [ "src/processing/TIFF.cpp", null, null ],
      [ "src/processing/TIFF.h", null, null ],
      [ "src/processing/TIFFTags.cpp", null, null ],
      [ "src/processing/TIFFTags.h", null, null ],
      [ "tests/testCapabilities.cpp", null, null ],
      [ "tests/testClassStructure.cpp", null, null ],
      [ "tests/testDemosaic.cpp", null, null ],
      [ "tests/testDNG.cpp", null, null ],
      [ "tests/testF2.cpp", null, null ],
      [ "tests/testF2Lens.cpp", null, null ],
      [ "tests/testFlashLatency.cpp", null, null ],
      [ "tests/testImage.cpp", null, null ],
      [ "tests/testJPEG.cpp", null, null ],
      [ "tests/testRestart.cpp", null, null ],
      [ "tests/testStream.cpp", null, null ],
      [ "tests/testTagValue.cpp", null, null ],
      [ "tests/testTSQueue.cpp", null, null ],
      [ "utils/fcamDngUtil.cpp", "fcam_dng_util_8cpp.html", null ],
      [ "utils/fcamVignette.cpp", "fcam_vignette_8cpp.html", null ],
      [ "utils/LinearAlgebra.h", null, null ]
    ] ],
    [ "File Members", "globals.html", null ]
  ] ]
];

function createIndent(o,domNode,node,level)
{
  if (node.parentNode && node.parentNode.parentNode)
  {
    createIndent(o,domNode,node.parentNode,level+1);
  }
  var imgNode = document.createElement("img");
  if (level==0 && node.childrenData)
  {
    node.plus_img = imgNode;
    node.expandToggle = document.createElement("a");
    node.expandToggle.href = "javascript:void(0)";
    node.expandToggle.onclick = function() 
    {
      if (node.expanded) 
      {
        $(node.getChildrenUL()).slideUp("fast");
        if (node.isLast)
        {
          node.plus_img.src = node.relpath+"ftv2plastnode.png";
        }
        else
        {
          node.plus_img.src = node.relpath+"ftv2pnode.png";
        }
        node.expanded = false;
      } 
      else 
      {
        expandNode(o, node, false);
      }
    }
    node.expandToggle.appendChild(imgNode);
    domNode.appendChild(node.expandToggle);
  }
  else
  {
    domNode.appendChild(imgNode);
  }
  if (level==0)
  {
    if (node.isLast)
    {
      if (node.childrenData)
      {
        imgNode.src = node.relpath+"ftv2plastnode.png";
      }
      else
      {
        imgNode.src = node.relpath+"ftv2lastnode.png";
        domNode.appendChild(imgNode);
      }
    }
    else
    {
      if (node.childrenData)
      {
        imgNode.src = node.relpath+"ftv2pnode.png";
      }
      else
      {
        imgNode.src = node.relpath+"ftv2node.png";
        domNode.appendChild(imgNode);
      }
    }
  }
  else
  {
    if (node.isLast)
    {
      imgNode.src = node.relpath+"ftv2blank.png";
    }
    else
    {
      imgNode.src = node.relpath+"ftv2vertline.png";
    }
  }
  imgNode.border = "0";
}

function newNode(o, po, text, link, childrenData, lastNode)
{
  var node = new Object();
  node.children = Array();
  node.childrenData = childrenData;
  node.depth = po.depth + 1;
  node.relpath = po.relpath;
  node.isLast = lastNode;

  node.li = document.createElement("li");
  po.getChildrenUL().appendChild(node.li);
  node.parentNode = po;

  node.itemDiv = document.createElement("div");
  node.itemDiv.className = "item";

  node.labelSpan = document.createElement("span");
  node.labelSpan.className = "label";

  createIndent(o,node.itemDiv,node,0);
  node.itemDiv.appendChild(node.labelSpan);
  node.li.appendChild(node.itemDiv);

  var a = document.createElement("a");
  node.labelSpan.appendChild(a);
  node.label = document.createTextNode(text);
  a.appendChild(node.label);
  if (link) 
  {
    a.href = node.relpath+link;
  } 
  else 
  {
    if (childrenData != null) 
    {
      a.className = "nolink";
      a.href = "javascript:void(0)";
      a.onclick = node.expandToggle.onclick;
      node.expanded = false;
    }
  }

  node.childrenUL = null;
  node.getChildrenUL = function() 
  {
    if (!node.childrenUL) 
    {
      node.childrenUL = document.createElement("ul");
      node.childrenUL.className = "children_ul";
      node.childrenUL.style.display = "none";
      node.li.appendChild(node.childrenUL);
    }
    return node.childrenUL;
  };

  return node;
}

function showRoot()
{
  var headerHeight = $("#top").height();
  var footerHeight = $("#nav-path").height();
  var windowHeight = $(window).height() - headerHeight - footerHeight;
  navtree.scrollTo('#selected',0,{offset:-windowHeight/2});
}

function expandNode(o, node, imm)
{
  if (node.childrenData && !node.expanded) 
  {
    if (!node.childrenVisited) 
    {
      getNode(o, node);
    }
    if (imm)
    {
      $(node.getChildrenUL()).show();
    } 
    else 
    {
      $(node.getChildrenUL()).slideDown("fast",showRoot);
    }
    if (node.isLast)
    {
      node.plus_img.src = node.relpath+"ftv2mlastnode.png";
    }
    else
    {
      node.plus_img.src = node.relpath+"ftv2mnode.png";
    }
    node.expanded = true;
  }
}

function getNode(o, po)
{
  po.childrenVisited = true;
  var l = po.childrenData.length-1;
  for (var i in po.childrenData) 
  {
    var nodeData = po.childrenData[i];
    po.children[i] = newNode(o, po, nodeData[0], nodeData[1], nodeData[2],
        i==l);
  }
}

function findNavTreePage(url, data)
{
  var nodes = data;
  var result = null;
  for (var i in nodes) 
  {
    var d = nodes[i];
    if (d[1] == url) 
    {
      return new Array(i);
    }
    else if (d[2] != null) // array of children
    {
      result = findNavTreePage(url, d[2]);
      if (result != null) 
      {
        return (new Array(i).concat(result));
      }
    }
  }
  return null;
}

function initNavTree(toroot,relpath)
{
  var o = new Object();
  o.toroot = toroot;
  o.node = new Object();
  o.node.li = document.getElementById("nav-tree-contents");
  o.node.childrenData = NAVTREE;
  o.node.children = new Array();
  o.node.childrenUL = document.createElement("ul");
  o.node.getChildrenUL = function() { return o.node.childrenUL; };
  o.node.li.appendChild(o.node.childrenUL);
  o.node.depth = 0;
  o.node.relpath = relpath;

  getNode(o, o.node);

  o.breadcrumbs = findNavTreePage(toroot, NAVTREE);
  if (o.breadcrumbs == null)
  {
    o.breadcrumbs = findNavTreePage("index.html",NAVTREE);
  }
  if (o.breadcrumbs != null && o.breadcrumbs.length>0)
  {
    var p = o.node;
    for (var i in o.breadcrumbs) 
    {
      var j = o.breadcrumbs[i];
      p = p.children[j];
      expandNode(o,p,true);
    }
    p.itemDiv.className = p.itemDiv.className + " selected";
    p.itemDiv.id = "selected";
    $(window).load(showRoot);
  }
}

